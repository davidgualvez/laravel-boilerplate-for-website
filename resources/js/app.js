require('./bootstrap');
import Vue from 'vue'

Vue.component('template-container', require('./components/template/Container.vue').default);

const app = new Vue({
    el: '#app'
});
